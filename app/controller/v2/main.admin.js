import {
  filterItems,
  getEle,
  layThongTinTuForm,
  renderTable,
  resetSpanNone,
  showThongTinLenForm,
  spanBlock,
  validator,
} from "./controller.admin.js";

let BASE_URL = "https://63063e00c0d0f2b801191139.mockapi.io";

let productList = [];

// hàm lấy data từ sercer
let renderTableService = () => {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then((res) => {
      productList = res.data;

      renderTable(productList);
    })
    .catch((err) => {});
};

// gọi api
renderTableService();

// chức năng fillter
getEle("selected").addEventListener("change", function () {
  let option = getEle("selected").value;
  if (option == "All") {
    renderTable(productList);
  } else if (option == "Iphone") {
    let iphoneList = filterItems(productList, option);
    renderTable(iphoneList);
  } else if (option == "Samsung") {
    let samSungList = filterItems(productList, option);
    renderTable(samSungList);
  }
});

// chức năng xóa
let btnXoa = (id) => {
  console.log(id);
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      renderTableService(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.btnXoa = btnXoa;

// chức năng sửa
let btnSua = (id) => {
  getEle("txt-id").disabled = true;
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.btnSua = btnSua;

// chức năng cập nhật
let btnUpdate = () => {
  let dataForm = layThongTinTuForm();
  // validate name
  let isValid = validator.kiemTraInputRong(
    dataForm.name,
    "vldName",
    "Trường này không để rỗng "
  );
  // validate loại
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.type,
      "vldType",
      "Trường này không để rỗng"
    );
  // validate giá
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.price,
      "vldPrice",
      "Trường này không để rỗng"
    );
  // validate cam trước
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.frontCamera,
      "vldFrontCamera",
      "Trường này không để rỗng"
    );
  // validate cam sau
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.backCamera,
      "vldBackCamera",
      "Trường này không để rỗng"
    );
  // validate screen
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.screen,
      "vldScreen",
      "Trường này không để rỗng"
    );
  // validate img
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.img,
      "vldImg",
      "Trường này không để rỗng"
    );
  // validate desc
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.desc,
      "vldDesc",
      "Trường này không để rỗng"
    );

  if (isValid == false) {
    $("#exampleModal").modal("show");
    return;
  } else {
    $("#exampleModal").modal("hide");
  }
  axios({
    url: `${BASE_URL}/product/${dataForm.id}`,
    method: "PUT",
    data: dataForm,
  })
    .then((res) => {
      renderTableService(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.btnUpdate = btnUpdate;

// chức năng thêm
let btnModal = () => {
  getEle("txt-id").disabled = true;
  getEle("modalForm").reset();
};
window.btnModal = btnModal;
let btnAddSP = () => {
  let dataForm = layThongTinTuForm();
  spanBlock();
  // validate name
  let isValid = validator.kiemTraInputRong(
    dataForm.name,
    "vldName",
    "Trường này không để rỗng "
  );
  // validate loại
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.type,
      "vldType",
      "Trường này không để rỗng"
    );
  // validate giá
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.price,
      "vldPrice",
      "Trường này không để rỗng"
    );
  // validate cam trước
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.frontCamera,
      "vldFrontCamera",
      "Trường này không để rỗng"
    );
  // validate cam sau
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.backCamera,
      "vldBackCamera",
      "Trường này không để rỗng"
    );
  // validate screen
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.screen,
      "vldScreen",
      "Trường này không để rỗng"
    );
  // validate img
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.img,
      "vldImg",
      "Trường này không để rỗng"
    );
  // validate desc
  isValid =
    isValid &
    validator.kiemTraInputRong(
      dataForm.desc,
      "vldDesc",
      "Trường này không để rỗng"
    );

  if (isValid == false) {
    $("#exampleModal").modal("show");
    return;
  } else {
    $("#exampleModal").modal("hide");
  }
  axios({
    url: `${BASE_URL}/product`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      renderTableService(dataForm);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.btnAddSP = btnAddSP;

// chức năng reset form
let btnReset = () => {
  getEle("modalForm").reset();
  resetSpanNone();
};
window.btnReset = btnReset;
