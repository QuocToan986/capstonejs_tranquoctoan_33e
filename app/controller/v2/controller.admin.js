import { Procduct } from "../../model/productModel.js";

// hàm DOM
export let getEle = (id) => {
  return document.getElementById(id);
};

// hàm render table
export let renderTable = (list) => {
  let contentHTML = "";
  list.forEach((ele) => {
    contentHTML += `
    <tr>
    <th scope="row">${ele.id}</th>
    <td>
      <img
        style="width: 80px"
        src= ${ele.img}
        alt=""
      />
    </td>
    <td style = "">${ele.name}</td>
    <td>$${ele.price.toLocaleString()}</td>
    <td>
    +${ele.screen} <br/>
    +Camera trước: ${ele.frontCamera} <br/>
    +Camera sau: ${ele.backCamera} <br/>
    +${ele.desc}
    </td>    
    <td>
      <button class="btn btn-danger" onclick = "btnXoa(${ele.id})">Xóa</button>
      <button class="btn btn-warning" onclick = "btnSua(${ele.id})">Sửa</button>
    </td>
  </tr>
    `;
  });
  getEle("tbody").innerHTML = contentHTML;
};

// hàm fillter
export let filterItems = (arr, query) => {
  return arr.filter((el) =>
    el.type.toLowerCase().includes(query.toLowerCase())
  );
};

// hàm show thông tin lên form
export let showThongTinLenForm = (data) => {
  getEle("txt-id").value = data.id;
  getEle("txt-name").value = data.name;
  getEle("txt-type").value = data.type;
  getEle("txt-price").value = data.price;
  getEle("txt-img").value = data.img;
  getEle("txt-desc").value = data.desc;
  getEle("txt-screen").value = data.screen;
  getEle("txt-frontCamera").value = data.frontCamera;
  getEle("txt-backCamera").value = data.backCamera;
};

// hàm lấy thông tin từ form
export let layThongTinTuForm = () => {
  let id = getEle("txt-id").value.trim();
  let name = getEle("txt-name").value.trim();
  let price = getEle("txt-price").value.trim();
  let frontCamera = getEle("txt-frontCamera").value.trim();
  let backCamera = getEle("txt-backCamera").value.trim();
  let screen = getEle("txt-screen").value.trim();
  let img = getEle("txt-img").value.trim();
  let desc = getEle("txt-desc").value.trim();
  let type = getEle("txt-type").value.trim();
  return new Procduct(
    id,
    name,
    price,
    screen,
    frontCamera,
    backCamera,
    img,
    desc,
    type
  );
};

// reset span
export let resetSpanNone = () => {
  getEle("vldName").style.display = "none";
  getEle("vldType").style.display = "none";
  getEle("vldPrice").style.display = "none";
  getEle("vldFrontCamera").style.display = "none";
  getEle("vldBackCamera").style.display = "none";
  getEle("vldScreen").style.display = "none";
  getEle("vldImg").style.display = "none";
  getEle("vldDesc").style.display = "none";
};
export let spanBlock = () => {
  getEle("vldName").style.display = "block";
  getEle("vldType").style.display = "block";
  getEle("vldPrice").style.display = "block";
  getEle("vldFrontCamera").style.display = "block";
  getEle("vldBackCamera").style.display = "block";
  getEle("vldScreen").style.display = "block";
  getEle("vldImg").style.display = "block";
  getEle("vldDesc").style.display = "block";
};

// validate
export let validator = {
  kiemTraInputRong: function (valueInput, idError, message) {
    if (valueInput == "") {
      getEle(idError).innerHTML = message;
      document.querySelectorAll("#modelForm span").display = "block";
      return false;
    } else {
      getEle(idError).innerText = "";
      document.querySelectorAll("#modelForm span").display = "none";
      return true;
    }
  },
};
