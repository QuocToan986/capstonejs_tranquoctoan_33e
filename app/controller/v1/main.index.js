import {
  addToCart,
  addToCartItem,
  deleteProduct,
  minusQuantity,
  plusQuantity,
  renderItem,
  renderTableCart,
  getEle,
  filterItems,
  totalMoney,
} from "../v1/controller.index.js";

let cartLocalStorage = localStorage.getItem("Product");
if (JSON.parse(cartLocalStorage)) {
  let product = JSON.parse(cartLocalStorage);
  renderTableCart(product);
}

let BASE_URL = "https://63063e00c0d0f2b801191139.mockapi.io";

let productList = [];

let cart = [];

// hàm lấy data từ server
let renderProductService = () => {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then((res) => {
      productList = res.data;
      renderItem(productList);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};

// chức năng gọi api
renderProductService();

// chức năng đặt hàng
let btnOder = (id) => {
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      let newItem = addToCartItem(res.data);
      addToCart(cart, newItem);
      renderTableCart(cart);
      let cartJson = saveLocalStorage(cart);
      localStorage.setItem("Product", cartJson);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.btnOder = btnOder;

// chức năng tăng số lượng
let btnAdd = (id) => {
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      plusQuantity(cart, res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.btnAdd = btnAdd;

// chức năng giảm số lượng
let btnMinus = (id) => {
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      minusQuantity(cart, res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.btnMinus = btnMinus;

// chức năng fillter
getEle("selected").addEventListener("change", function () {
  let option = getEle("selected").value;
  if (option == "All") {
    renderItem(productList);
  } else if (option == "Iphone") {
    let iphoneList = filterItems(productList, option);
    renderItem(iphoneList);
  } else if (option == "Samsung") {
    let samSungList = filterItems(productList, option);
    renderItem(samSungList);
  }
});

// chức năng xóa
let btnRemove = (id) => {
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      deleteProduct(cart, res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.btnRemove = btnRemove;

//chức năng thanh toán
let btnPay = () => {
  cart = [];
  totalMoney(cart);
  renderTableCart(cart);
};
window.btnPay = btnPay;

let saveLocalStorage = (cart) => {
  let cartJson = "";
  cartJson = JSON.stringify(cart);
  return cartJson;
};
