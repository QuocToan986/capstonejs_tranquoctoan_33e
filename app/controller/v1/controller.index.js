// hàm DOM
export let getEle = (id) => {
  return document.getElementById(id);
};

// hàm render list item
export let renderItem = (list) => {
  let contentHTM = "";
  list.forEach((item) => {
    contentHTM += /*html*/ `
    <div class="product-item">
    <img
      src=${item.img}
      alt=""
      width: 20px
    />
    <div class="product-desc">
    <p>${item.name}</p>
    <span class = "price">$${item.price.toLocaleString()}</span> <br/>
    <span class = "screen">+${item.screen}</span> <br/>
    <span class = "camFront">+Camera trước: ${item.frontCamera}</span>; <br/>
    <span class = "camBack">+${item.backCamera}</span>;<br/>
    <span class = "desc">+${item.desc}</span>
    </div>
    <div style="text-align: center; ">
      <button class = "btn btn-success mt-3" style = "width: 100%" onclick = "btnOder(${
        item.id
      })">Đặt hàng</button>
    </div>
  </div>
    `;
  });
  document.querySelector(".product-list").innerHTML = contentHTM;
};

// hàm render tablecart
export let renderTableCart = (list) => {
  let content = "";
  for (let index = 0; index < list.length; index++) {
    let product = list[index].product;
    let quantity = list[index].quantity;
    let total = list[index].total;
    content += /*html*/ `
    <tr>
    <th scope="row" style = "text-align: left">
      <p>
        <img style="width: 60px"
          src=${product.img}
          alt="" />
        ${product.name}
      </p>
    </th>
    <td>$${product.price}</td>
    <td>
      <button style="padding: 0px 4px;" onclick = "btnMinus(${
        product.id
      })" id= "btn-minus">
        <span><i class="fa fa-minus"></i></span>
      </button>
      <input type="text" name="" id="txt-so-luong" value=${quantity} style="width: 40px; padding-left: 14px;" />
      <button style="padding: 0px 4px;" onclick = "btnAdd(${
        product.id
      })" id= "btn-plus">
        <span><i class="fa fa-plus"></i></span>
      </button>
    </td>
    <td>$${total.toLocaleString()}</td>
    <td> <button class = "btn btn-danger" onclick="btnRemove(${
      product.id
    })"><i class="fa fa-trash" ></i></button></td>

  </tr>
    `;
  }
  getEle("tableCart").innerHTML = content;
};

// hàm fillter
export let filterItems = (arr, query) => {
  return arr.filter((el) =>
    el.type.toLowerCase().includes(query.toLowerCase())
  );
};

// add dô cartItem
export let addToCartItem = (item) => {
  let cartItem = {
    product: item,
    quantity: 1,
    total: parseInt(item.price),
  };
  return cartItem;
};

// cartItem add do cart
export let addToCart = (cart, cartItem) => {
  if (cart == "") {
    cart.push(cartItem);
  } else {
    checkCart(cart, cartItem);
  }
  totalMoney(cart);
};
let checkCart = (cart, cartItem) => {
  let index = findIndex(cart, cartItem);
  if (index !== -1) {
    cart[index].quantity += 1;
    cart[index].total =
      parseInt(cart[index].quantity) * parseInt(cart[index].product.price);
  } else {
    cart.push(cartItem);
  }
};
export let totalMoney = (cart) => {
  let totalMoney = 0;
  for (let i = 0; i < cart.length; i++) {
    let item = cart[i];
    totalMoney += item.total;
  }
  getEle("totalMoney").innerText = `$${totalMoney.toLocaleString()}`;
};

let findIndex = (cart, cartItem) => {
  for (let index = 0; index < cart.length; index++) {
    let item = cart[index];
    if (item.product.id == cartItem.product.id) {
      return index;
    }
  }
  return -1;
};

// plus quantity
export let plusQuantity = (cart, data) => {
  let index = findIndexCart(cart, data);
  if (index !== -1) {
    cart[index].quantity += 1;
    cart[index].total =
      parseInt(cart[index].quantity) * parseInt(cart[index].product.price);
    totalMoney(cart);
    renderTableCart(cart);
  }
};

// minus quantity
export let minusQuantity = (cart, data) => {
  let index = findIndexCart(cart, data);
  if (index !== -1) {
    cart[index].quantity -= 1;
    cart[index].total =
      parseInt(cart[index].quantity) * parseInt(cart[index].product.price);
    totalMoney(cart);
    renderTableCart(cart);
  }
  if (cart[index].quantity <= 0) {
    cart.splice(index, 1);
    renderTableCart(cart);
  }
};

// remove product
export let deleteProduct = (cart, data) => {
  let index = findIndexCart(cart, data);
  if (index !== -1) {
    cart.splice(index, 1);
    totalMoney(cart);
    renderTableCart(cart);
  }
};
let findIndexCart = (cart, data) => {
  for (let index = 0; index < cart.length; index++) {
    let element = cart[index];
    if (element.product.id == data.id) {
      return index;
    }
  }
  return -1;
};
